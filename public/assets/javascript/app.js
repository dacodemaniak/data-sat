/**
 * @name app.js
 * @abstract Point d'entrée Javascript
 */

$(document).ready(() => {
	console.info('JS app is running');
	
	// Tableau des résultats retournés
	let results = [];
	
	// Handling search-form
	$('#search-form').on(
		'submit', // Evénement à écouter
		(event) => { // Gestionnaire de l'événement
			$('#data-loader').removeClass('silent');
			
			event.preventDefault(); // Interdire l'événement par défaut
			
			// Call Symfony API with parameters
			$.ajax({
				url: 'http://data-sat.wrk/results/' + $('#begin').val() + '/' + $('#end').val(),
				method: 'get',
				dataType: 'json',
				success: (datas) => {
					// Clear the tbody tr
					$('table#results tbody tr').remove();
					
					// Display rows number in the tfoot
					$('#results-number').html(datas.length);
					// Then... loop over the results
					let indice = 0;
					datas.forEach((result) => {
						results.push(result); // Ajoute un résultat à la collection
						
						let row = $('<tr>');
						// Ajouter un attribut à la ligne courante
						row.attr('data-rel', indice);
						
						indice++;
						
						// First column : id
						let column = $('<td>');
						column.html(result.id);
						column.appendTo(row);
						
						// Second column : Date
						column = $('<td>');
						let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
						column.html(new Date(result.date).toLocaleDateString('fr-FR', options));
						column.appendTo(row);
						
						// Third column : Nombre de fichiers...
						column = $('<td>');
						column.html(result.fileNumber);
						column.appendTo(row);
						
						// Add the whole row to existing DOM element
						$('table#results tbody').append(row);
					});
					$('#data-loader').addClass('silent');
				},
				error: (xhr, error) => {
					console.log('API call failed ', error);
					$('#data-loader').addClass('silent');
				}
			});
		}
	);
	
	// Détection d'un click sur une ligne du tableau
	$('table#results tbody').on(
		'click',
		'tr', // Délegation d'événement...
		(event) => {
			console.log('My logic here on : ', $(event.target).parent('tr').attr('data-rel'));
			const indice = $(event.target).parent('tr').attr('data-rel');
			const result = results[indice].files;
			console.log('Files :', result);
		}
	);
});

