<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Results;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\JsonResponse;

class ResultsController extends Controller
{
    private $category;
    
    /**
     * @Route(
     *  path="results/{categoryId}",
     *  name="category_results",
     *  methods={"GET"})
     *
     */
    public function index($categoryId)
    {
        $this->category = $this->getResults($categoryId);
        $results = $this->getMonthlyResults();
        
        return $this->render('results/index.html.twig', [
            "category" => $this->category,
            "between" => $results,
            "begin" => new \DateTime()
        ]);
    }

    /**
     * @Route(
     *  path="results",
     *  name="get_results",
     *  methods={"GET"})
     *
     */
    public function getAll() {
        return $this->json(
            $this->getDoctrine()
                ->getManager()
                ->getRepository(Results::class)
                ->findAll()
        );
    }
    
    /**
     * @Route(
     *  path="results/{begin}/{end}",
     *  name="date_results",
     *  methods={"GET"})
     * 
     * @param Request $request
     */
    public function results($begin, $end) {
        
        
        $results = $this->getMonthlyResults(new \DateTime($begin));
        
        $response = [
            "category" => "Résultats",
            "results" => $results
        ];
        
        return $this->json(
            $results,
            Response::HTTP_OK
        );
    }
    
    private function getResults(int $categoryId) {
        $em = $this->getDoctrine()->getManager();
        
        $results = $em->getRepository(Category::class)
            ->find($categoryId);
        
        return $results;
    }
    
    public function getMonthlyResults(\DateTime $begin = null): array {
        return $this->getDoctrine()->getRepository(Results::class)
            ->findBetween($begin);
    }
}
