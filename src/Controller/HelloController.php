<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Category;

class HelloController extends AbstractController
{
    
    /**
     * Titre qui sera affiché dans l'onglet du navigateur
     * 
     * @var string
     */
    private $title = "Accueil";
    
    private $today;
    
    private $entityManager;
    
    public function __construct() {
        $this->today = new \DateTime();
    }
    
    /**
     * @Route("/hello", name="hello")
     */
    public function index()
    {
        return new Response("Hello Symfony, nice to meet");
    }
    
    /**
     * @Route("/hello/{name}", name="hello-name")
     * 
     * @return Response
     */
    public function helloWho($name): Response {
        
        return $this->render(
            "home/myhome.html.twig",
            [
                "title" => $this->title,
                "name" => $name
            ]
        );
    }
    
    /**
     * @Route("/", name="home")
     */
    public function home() {
        $this->entityManager = $this->getDoctrine()->getManager();
        
        return $this->render(
            "home/home.html.twig",
            [
                "title" => $this->title,
                "categories" => $this->getCategories(),
                "begin" => new \DateTime()
            ]
        );
    }
    
    private function getDatas(): array {
        $today = new \DateTime();
        
        $datas = [clone $today]; // Tableau avec la date du jour...
        
        for ($i=0; $i < 10; $i++) {
            $today->modify("-1 day");
            
            $datas[] = clone $today;
        }
        return $datas;
    }
    
    private function getCategories() {
        
        return $this->entityManager
            ->getRepository(Category::class)
            ->findAll();
    }
}
